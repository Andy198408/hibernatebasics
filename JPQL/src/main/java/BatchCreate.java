import entity.Beer;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.logging.Logger;


public class BatchCreate {

    private static Logger logger = Logger.getLogger("BatchCreate");

    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
//        logger.info("hello");

        try {
            transaction.begin();

            int batchSize = 10;
            int entitySize = 50;

            for (int id = 0; id < entitySize; id++) {
                if (id > 0 && id % batchSize == 0) {
                    //flush a batch of inserts and release memory
                    entityManager.flush();
                    entityManager.clear();
                }
                createBeers(entityManager, id);
            }


            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }

    private static void createBeers(EntityManager entityManager, int id) {
        Beer beer = new Beer();
        beer.setName("hibernate " + (id + 1));
        beer.setPrice(5D + id);
        beer.setAlcohol((double) createAlcohol(id));
        beer.setStock(100);

        entityManager.persist(beer);
//        logger.info("Beer with id: " + beer.getId() + "is persisted!");
    }

    private static int createAlcohol(int id) {
        if (id < 10) {
            return 5;
        } else if (id < 20) {
            return 6;
        } else if (id < 30) {
            return 8;
        } else if (id < 40) {
            return 8;
        } else if (id < 50) {
            return 9;
        } else {
            return 10;
        }
    }
}
