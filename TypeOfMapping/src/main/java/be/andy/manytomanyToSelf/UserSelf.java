package be.andy.manytomanyToSelf;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "USERSSELF")
public class UserSelf {
    private Long id;
    private String username;
    private String password;
    private String email;

    private Set<UserSelf> userTo = new HashSet<>();
    private Set<UserSelf> userFrom = new HashSet<>();

    public UserSelf(String username, String password, String email) {
        this.username = username;
        this.password = password;
        this.email = email;
    }

    public UserSelf() {

    }

    @Id
    @GeneratedValue
    @Column(name = "USER_ID")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @ManyToMany()
    @JoinTable(name = "tbl_friends",
            joinColumns = @JoinColumn(name = "personId"),
            inverseJoinColumns = @JoinColumn(name = "friendId")
    )
    public Set<UserSelf> getUserTo() {
        return userTo;
    }

    public void setUserTo(Set<UserSelf> userTo) {
        this.userTo = userTo;
    }

    @ManyToMany(mappedBy = "userTo")
    public Set<UserSelf> getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(Set<UserSelf> userFrom) {
        this.userFrom = userFrom;
    }

    public void addFriend(UserSelf user) {

    }

    @Override
    public String toString() {
        return "UserSelf{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
