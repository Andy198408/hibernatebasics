package be.andy.OneToOne.OnForeignKey;

import be.andy.OneToOne.OnForeignKey.entity.LibraryOTO;
import be.andy.OneToOne.OnForeignKey.entity.StudentOTO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class onFKInsert {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            StudentOTO studentOTO = entityManager.find(StudentOTO.class, 1L);
            LibraryOTO libraryOTO = new LibraryOTO();
            libraryOTO.setB_name("Another day of hibernate");
            libraryOTO.setStud(studentOTO);
            entityManager.persist(libraryOTO);

            transaction.commit();
        } finally {
            if(transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
