package be.andy.ManyToOne.Bidirectional.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class LibraryMTOBi {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int b_id;
    private String b_name;

    @OneToMany(mappedBy = "libraryMTOBi")
    private List<StudentMTOBi> students = new ArrayList<>();

    public LibraryMTOBi(int b_id, String b_name, StudentMTOBi stud) {
        super();
        this.b_id = b_id;
        this.b_name = b_name;
    }

    public LibraryMTOBi() {
        super();
    }

    public int getB_id() {
        return b_id;
    }

    public void setB_id(int b_id) {
        this.b_id = b_id;
    }

    public String getB_name() {
        return b_name;
    }

    public void setB_name(String b_name) {
        this.b_name = b_name;
    }

    public void addStudent(StudentMTOBi studentMTOBi){
        this.students.add(studentMTOBi);
        studentMTOBi.setLibraryMTO(this);
    }
    public void removeStudent(StudentMTOBi studentMTOBi){
        this.students.remove(studentMTOBi);
        studentMTOBi.setLibraryMTO(null);
    }
}


