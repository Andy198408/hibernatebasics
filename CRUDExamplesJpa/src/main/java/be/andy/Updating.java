package be.andy;

import entity.Products;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class Updating {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();

        Products foundProd = entityManager.find(Products.class, 6);
        System.out.println("Before update!");
        System.out.println(foundProd);

        foundProd.setPrice(66.6);
        foundProd.setName("JpaTestUpdate");

        System.out.println("After update!");
        System.out.println(foundProd);

        try {
            transaction.begin();

            entityManager.persist(foundProd);

            transaction.commit();
        } finally {
            if(transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }



    }
}
