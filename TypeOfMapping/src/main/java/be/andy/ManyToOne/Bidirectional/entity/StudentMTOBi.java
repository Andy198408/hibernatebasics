package be.andy.ManyToOne.Bidirectional.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
public class StudentMTOBi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int s_id;
    private String s_name;

    @ManyToOne(fetch = FetchType.LAZY)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private LibraryMTOBi libraryMTOBi;

    public LibraryMTOBi getLibraryMTO() {
        return libraryMTOBi;
    }

    public void setLibraryMTO(LibraryMTOBi libraryMTOBi) {
        this.libraryMTOBi = libraryMTOBi;
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }


}
