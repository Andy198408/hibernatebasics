package be.andy;

import be.andy.model.Post;
import be.andy.repository.PostRepository;

public class mainApp {
    public static void main(String... args) {
        Long postId = 1L;
        Post post = null;
        PostRepository postRepository = new PostRepository();

        //Using EntityManager.find().
        System.out.println("Using em find: ");
        post = postRepository.find(postId);
//        System.out.println(post.getUser());
        System.out.println("Using em findWithEntityGraph: ");
        post = postRepository.findWithEntityGraph(postId);
//        System.out.println(post.getUser());
        System.out.println("Using em findWithEntityGraph2: ");
        post = postRepository.findWithEntityGraph2(postId);

        //Using JPQL: Query and TypedQuery
        System.out.println("Using em findUsingJpql: ");
        post = postRepository.findUsingJpql(postId);

        //Using Criteria API
        System.out.println("Using em findUsingCriteria: ");
        post = postRepository.findUsingCriteria(postId);

        postRepository.clean();
    }
}
