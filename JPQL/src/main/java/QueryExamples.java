import entity.Beer;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class QueryExamples {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            System.out.println("So it begins!");
            System.out.println("Named query result beer by name with namedQuery example: ");
            Query getBeerByName = entityManager.createNamedQuery("get_beer_by_name")
                    .setParameter("name", "hibernate%");

            List beerByNameResultList = getBeerByName.getResultList();
            System.out.println(beerByNameResultList);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            System.out.println("Named query result beer by stock with resultstream example: ");

            Query getBeerByStock = entityManager.createNamedQuery("get_beer_by_stock", Beer.class)
                    .setParameter(1, 100);

            Stream<Beer> beerByStockResultStream = getBeerByStock.getResultStream();

            List<Beer> beers = beerByStockResultStream
                    .skip(5L)
                    .limit(3L)
                    .collect((Collectors.toList()));

            beers.forEach(beer -> {
                beer.setName("touched by stream");
                System.out.println(beer);
            });
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            System.out.println("Beer by alcohol with resultlist: ");

            Query queryAlcohol = entityManager.createQuery(
                    "select b" +
                            " from Beer b " +
                            "where b.alcohol = :alcohol ")
                    .setParameter("alcohol", 6D);
            List queryAlcoholResultList = queryAlcohol.getResultList();
            queryAlcoholResultList.forEach(System.out::println);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            System.out.println("Typed query example with multiple select expression example: ");
            TypedQuery<Object[]> typedQuery = entityManager.createQuery(
                    "select b.name, b.alcohol " +
                            "from Beer b " +
                            "where b.alcohol = :alcohol", Object[].class)
                    .setParameter("alcohol", 6D);
            List<Object[]> typedQueryResultList = typedQuery.getResultList();
            for (Object[] objects : typedQueryResultList) {
                System.out.printf(
                        "Retrieved from select expression, name: %s alcohol: %.2f%n", objects[0], objects[1]
                );
            }
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            System.out.println("Typed query example with constructor expressions example: ");
            TypedQuery<Beer> typedQueryConstructor = entityManager.createQuery(
                    "select new Beer(b.name, b.alcohol) " +
                            "from Beer b " +
                            "where b.alcohol = :alcohol", Beer.class)
                    .setParameter("alcohol", 6D);
            List<Beer> resultListConstructor = typedQueryConstructor.getResultList();
            resultListConstructor.forEach(System.out::println);
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
            System.out.println("Typed query example with singleresult example: ");
            TypedQuery<Beer> typedQuerySingle = entityManager.createQuery(
                    "select b " +
                            "from Beer b " +
                            "where b.id = ?1", Beer.class)
                    .setParameter(1, 1);

            Beer typedQuerySingleResult = typedQuerySingle.getSingleResult();
            System.out.println(typedQuerySingleResult);


            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
