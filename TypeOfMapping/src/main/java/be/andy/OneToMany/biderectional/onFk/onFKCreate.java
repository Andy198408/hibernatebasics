package be.andy.OneToMany.biderectional.onFk;

import be.andy.OneToMany.biderectional.onFk.entity.LibraryOTMBi;
import be.andy.OneToMany.biderectional.onFk.entity.StudentOTMBi;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;

public class onFKCreate {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            LibraryOTMBi lib1 = new LibraryOTMBi();
            lib1.setB_name("Data Structure");

            LibraryOTMBi lib2 = new LibraryOTMBi();
            lib2.setB_name("DBMS");

//            ArrayList<LibraryOTMBi> list = new ArrayList<LibraryOTMBi>();
//            list.add(lib1);
//            list.add(lib2);

            StudentOTMBi st1 = new StudentOTMBi();
            st1.setS_name("Andy");
            st1.addBook(lib1);
            st1.addBook(lib2);

            entityManager.persist(st1);

            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
