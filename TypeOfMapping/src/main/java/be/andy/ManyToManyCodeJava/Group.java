package be.andy.ManyToManyCodeJava;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "GROUPS")
public class Group {
    private Long id;
    private String name;

    private Set<User> users = new HashSet<>();

    public Group(String name) {
        this.name = name;
    }

    public Group() {

    }

    public void addUser(User user) {
        this.users.add(user);
    }

    @Id
    @GeneratedValue
    @Column(name = "GROUP_ID")
    public Long getId() {
        return id;
    }

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "USERS_GROUPS",
            joinColumns = @JoinColumn(name = "GROUP_ID"),
            inverseJoinColumns = @JoinColumn(name = "USER_ID")
    )
    public Set<User> getUsers() {
        return users;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
