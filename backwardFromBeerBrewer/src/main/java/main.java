import entity.Beers;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;


public class main {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            for (int id = 0; id < 5; id++) {
                createBeers(entityManager, id);

            }
            Beers beers = entityManager.find(Beers.class, 8);
            System.out.println(beers);


            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }

    private static void createBeers(EntityManager entityManager, int id) {
        Beers beers = new Beers();
        beers.setName("hibernate " + id);
        beers.setPrice(5D + id);
        beers.setAlcohol(5D + id);

        entityManager.persist(beers);
        System.out.println("Beer with id: " + beers.getId() + "is persisted!");
    }
}
