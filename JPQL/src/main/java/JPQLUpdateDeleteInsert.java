import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class JPQLUpdateDeleteInsert {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();
            System.out.println("Update statement used: ");
            int updatedEntities = entityManager.createQuery(
                    " update Beer b " +
                            " set b.name =:newName " +
                            " where b.id < 25 ")
//                    .setParameter("stock", 150)
                    .setParameter("newName", "updated < 25")
                    .executeUpdate();
            System.out.println("These are the updated rows: " + updatedEntities);

            System.out.println("Delete statement used: ");
            int deletedEntities = entityManager.createQuery(
                    " delete from Beer b " +
                            " where b.id > 25 or b.alcohol =:alcohol")
                    .setParameter("alcohol", 6D)
                    .executeUpdate();

            System.out.println("These are the deleted rows: " + deletedEntities);

            System.out.println("Insert statement used: ");
            //HQL is used here
            int insertedEntitiesBrewers = entityManager.unwrap(Session.class).createQuery(
                    "insert into Brewers (id, name) " +
                            "select b.id, b.name " +
                            "from Beer b " +
                            "where b.id=?1")
                    .setParameter(1, 2)
                    .executeUpdate();

            System.out.println("Inserted rows into brewers: " + insertedEntitiesBrewers);
            //this will only work if an id is generated auto increment in database. Not in memory!
            //Caused by: java.sql.SQLException: Field 'Id' doesn't have a default value
            int insertedEntitiesBeer = entityManager.createNativeQuery(
                    "insert into Beer (alcohol, name, price, stock) VALUES (?,?,?,?)")
                    .setParameter(1, 8D)
                    .setParameter(2, "insertNative")
                    .setParameter(3, 8D)
                    .setParameter(4, 8D)
                    .executeUpdate();

            System.out.println("Inserted rows into beers: " + insertedEntitiesBeer);


            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
