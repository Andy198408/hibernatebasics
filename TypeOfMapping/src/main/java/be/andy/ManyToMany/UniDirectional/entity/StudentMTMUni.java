package be.andy.ManyToMany.UniDirectional.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "studentmtm")
public class StudentMTMUni {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int s_id;
    private String s_name;
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "StudentMTM_LibraryMTM",
            joinColumns =
            @JoinColumn(name = "s_id"),
            inverseJoinColumns =
            @JoinColumn(name = "b_id")
    )
    private List<LibraryMTMUni> libraryMTMUni;

    public StudentMTMUni(String s_name, List libraryMTM) {
        this.s_name = s_name;
        this.libraryMTMUni = libraryMTM;
    }

    public StudentMTMUni() {

    }

    public List<LibraryMTMUni> getLibraryMTM() {
        return libraryMTMUni;
    }

    public void setLibraryMTM(List libraryMTM) {
        this.libraryMTMUni = libraryMTM;
    }

    public int getS_id() {
        return s_id;
    }

    public void setS_id(int s_id) {
        this.s_id = s_id;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }


}
