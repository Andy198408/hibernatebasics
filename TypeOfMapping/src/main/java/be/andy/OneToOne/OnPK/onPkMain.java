package be.andy.OneToOne.OnPK;


import be.andy.OneToOne.OnPK.entity.Product;
import be.andy.OneToOne.OnPK.entity.ProductDetail;
import org.hibernate.Session;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;

public class onPkMain {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        Session session = entityManager.unwrap(Session.class);
        EntityTransaction transaction = session.getTransaction();
        try {
            transaction.begin();

// creates a new product
            Product product = new Product();
            product.setName("test5");
            product.setDescription("Expensive, fuel-blackhole car");
            product.setPrice(30000);

            // creates product detail
            ProductDetail detail = new ProductDetail();
            detail.setPartNumber("anothertest5");
            detail.setDimension("2,0m x 1,9m x 1,5m");
            detail.setWeight(2500);
            detail.setManufacturer("Honda Automobile");
            detail.setOrigin("Japan");

            // sets the bi-directional association
            product.setProductDetail(detail);
            detail.setProduct(product);

            // persists the product
            session.save(product);

            // queries all products
            List<Product> listProducts = session.createQuery("from Product").list();
            for (Product aProd : listProducts) {
                String info = "Product: " + aProd.getName() + "\n";
                info += "\tDescription: " + aProd.getDescription() + "\n";
                info += "\tPrice: $" + aProd.getPrice() + "\n";

                ProductDetail aDetail = aProd.getProductDetail();
                info += "\tPart number: " + aDetail.getPartNumber() + "\n";
                info += "\tDimension: " + aDetail.getDimension() + "\n";
                info += "\tWeight: " + aDetail.getWeight() + "\n";
                info += "\tManufacturer: " + aDetail.getManufacturer() + "\n";
                info += "\tOrigin: " + aDetail.getOrigin() + "\n";

                System.out.println(info);
            }

            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
