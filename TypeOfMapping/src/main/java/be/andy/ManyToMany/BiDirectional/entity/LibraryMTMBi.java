package be.andy.ManyToMany.BiDirectional.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "librarymtm")
public class LibraryMTMBi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int b_id;
    private String b_name;

    @ManyToMany(mappedBy = "libraryMTMBi")
    private List<StudentMTMBi> studentMTMBi;

    public LibraryMTMBi(String b_name, List studentMTM) {
        this.b_name = b_name;
        this.studentMTMBi = studentMTM;
    }

    public LibraryMTMBi() {
        super();
    }

    public int getB_id() {
        return b_id;
    }

    public void setB_id(int b_id) {
        this.b_id = b_id;
    }

    public String getB_name() {
        return b_name;
    }

    public void setB_name(String b_name) {
        this.b_name = b_name;
    }

    public List<StudentMTMBi> getStudentMTM() {
        return studentMTMBi;
    }

    public void setStudentMTM(List<StudentMTMBi> studentMTMBi) {
        this.studentMTMBi = studentMTMBi;
    }
}


