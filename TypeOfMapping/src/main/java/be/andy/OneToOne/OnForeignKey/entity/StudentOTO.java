package be.andy.OneToOne.OnForeignKey.entity;

import javax.persistence.*;

@Entity
public class StudentOTO {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long s_id;
    private String s_name;

//    used for bidirectional association.
//    @OneToOne(cascade = CascadeType.ALL)
//    LibraryOTO libraryOTO;

    public Long getS_id() {
        return s_id;
    }

    public void setS_id(Long s_id) {
        this.s_id = s_id;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

//    public LibraryOTO getLibrary() {
//        return libraryOTO;
//    }
//
//    public void setLibrary(LibraryOTO libraryOTO) {
//        this.libraryOTO = libraryOTO;
//    }
}
