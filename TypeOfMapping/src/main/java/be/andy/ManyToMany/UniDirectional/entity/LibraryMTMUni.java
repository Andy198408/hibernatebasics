package be.andy.ManyToMany.UniDirectional.entity;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "librarymtm")
public class LibraryMTMUni {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int b_id;
    private String b_name;

    public LibraryMTMUni(String b_name, List studentMTM) {
        this.b_name = b_name;
    }

    public LibraryMTMUni() {
        super();
    }

    public int getB_id() {
        return b_id;
    }

    public void setB_id(int b_id) {
        this.b_id = b_id;
    }

    public String getB_name() {
        return b_name;
    }

    public void setB_name(String b_name) {
        this.b_name = b_name;
    }

}


