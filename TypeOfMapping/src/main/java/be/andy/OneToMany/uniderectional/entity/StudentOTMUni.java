package be.andy.OneToMany.uniderectional.entity;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class StudentOTMUni {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long s_id;
    private String s_name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "s_id")
    private List<LibraryOTMUni> books_issued = new ArrayList<>();

    public StudentOTMUni() {
    }

    public Long getS_id() {
        return s_id;
    }

    public void setS_id(Long s_id) {
        this.s_id = s_id;
    }

    public String getS_name() {
        return s_name;
    }

    public void setS_name(String s_name) {
        this.s_name = s_name;
    }

    public List<LibraryOTMUni> getBooks_issued() {
        return books_issued;
    }

    public void setBooks_issued(List<LibraryOTMUni> books_issued) {
        this.books_issued = books_issued;
    }

}
