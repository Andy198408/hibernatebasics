package be.andy.OneToOne.OnForeignKey;

import be.andy.OneToOne.OnForeignKey.entity.LibraryOTO;
import be.andy.OneToOne.OnForeignKey.entity.StudentOTO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class onFKCreate {
    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            StudentOTO st1 = new StudentOTO();
            st1.setS_name("Dandy");

            StudentOTO st2 = new StudentOTO();
            st2.setS_name("BAndy");

            LibraryOTO lib1 = new LibraryOTO();
            lib1.setB_name("Data Structure");
            lib1.setStud(st1);

            LibraryOTO lib2 = new LibraryOTO();
            lib2.setB_name("DBMS");
            lib2.setStud(st2);

//            st1.setLibrary(lib1);
//            st2.setLibrary(lib2);

            entityManager.persist(st1);
            entityManager.persist(st2);

            entityManager.persist(lib1);
            entityManager.persist(lib2);

            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
