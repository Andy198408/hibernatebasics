package be.andy.manytomanyToSelf;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Set;

public class UserUpdate {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        UserSelf userSelf = entityManager.find(UserSelf.class, 1L);
        UserSelf userOther = entityManager.find(UserSelf.class, 2L);

        //find userself friends:
        Set<UserSelf> user = userSelf.getUserTo();
        //find who userself is friend of:
        Set<UserSelf> userOp = userSelf.getUserFrom();

        //find who is userother friend of:
        Set<UserSelf> userFriend = userOther.getUserFrom();
        //find user other friends:
        Set<UserSelf> userFriendOp = userOther.getUserTo();


        System.out.println("Friend of user return: ");
        user.forEach(System.out::println);
        System.out.println("Friend opo of user return: ");
        userOp.forEach(System.out::println);

        System.out.println("User friend returning user: ");
        userFriend.forEach(System.out::println);
        System.out.println("User friendOpo returning user: ");
        userFriendOp.forEach(System.out::println);

        System.out.println("Update!");

//        EntityTransaction transaction = entityManager.getTransaction();
//        try {
//            transaction.begin();
//
//            userOther.getUserTo().add(userSelf);
//
//            transaction.commit();
//        } finally {
//            if (transaction.isActive()) {
//                transaction.rollback();
//            }
//            entityManager.close();
//            entityManagerFactory.close();
//        }
//        entityManager.refresh(userOther);
//        entityManager.refresh(userSelf);


        System.out.println("Friend of user return: ");
        user.forEach(System.out::println);
        System.out.println("Friend opo of user return: ");
        userOp.forEach(System.out::println);

        System.out.println("User friend returning user: ");
        userFriend.forEach(System.out::println);
        System.out.println("User friendOpo returning user: ");
        userFriendOp.forEach(System.out::println);

    }
}
