import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.Arrays;

public class Aggregate {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            Object[] beerStatistics = entityManager.createQuery(
                    "select " +
                            "count(b)," +
                            "sum(b.alcohol)," +
                            "min(b.alcohol)," +
                            "max(b.alcohol)," +
                            "avg(b.alcohol)" +
                            "from Beer b ", Object[].class)
                    .getSingleResult();

            System.out.println(Arrays.toString(beerStatistics));
            System.out.printf(
                    "number of beers: %d%n alcohol count: %.2f%n min alcohol %.2f%n max alcohol %.2f%n " +
                    "average alcohol %.2f%n",
                    beerStatistics[0], beerStatistics[1], beerStatistics[2], beerStatistics[3], beerStatistics[4]);

            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
