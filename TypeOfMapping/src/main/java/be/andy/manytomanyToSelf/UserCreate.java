package be.andy.manytomanyToSelf;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;

public class UserCreate {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            UserSelf userTo = new UserSelf();
            UserSelf userFrm = new UserSelf();

            entityManager.persist(userTo);
            entityManager.persist(userFrm);

            userTo.getUserTo().add(userFrm);
            userTo.getUserFrom().add(userFrm);

            transaction.commit();
        } finally {
            if(transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
