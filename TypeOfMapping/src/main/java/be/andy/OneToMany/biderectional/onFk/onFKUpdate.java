package be.andy.OneToMany.biderectional.onFk;

import be.andy.OneToMany.biderectional.onFk.entity.LibraryOTMBi;
import be.andy.OneToMany.biderectional.onFk.entity.StudentOTMBi;

import javax.persistence.*;

public class onFKUpdate {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

//            StudentOTMBi studentOTMBi = entityManager.find(StudentOTMBi.class, 1L);
            Query andy = entityManager.createQuery("select s from StudentOTMBi s where s.s_name = ?1")
                    .setParameter(1, "Andy");
            StudentOTMBi singleResult = (StudentOTMBi) andy.getSingleResult();

            LibraryOTMBi libraryOTMBi = singleResult.getBooks_issued().get(0);
            libraryOTMBi.setB_name("This is an updated book singleresult!");

            entityManager.persist(singleResult);

            transaction.commit();
        } finally {
            if(transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
