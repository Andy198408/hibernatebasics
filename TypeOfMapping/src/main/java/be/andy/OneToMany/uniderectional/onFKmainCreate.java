package be.andy.OneToMany.uniderectional;

import be.andy.OneToMany.uniderectional.entity.LibraryOTMUni;
import be.andy.OneToMany.uniderectional.entity.StudentOTMUni;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.ArrayList;

public class onFKmainCreate {
    public static void main(String[] args) {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("default");
        EntityManager entityManager = entityManagerFactory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        try {
            transaction.begin();

            LibraryOTMUni lib1 = new LibraryOTMUni();
            lib1.setB_name("Data Structure");

            LibraryOTMUni lib2 = new LibraryOTMUni();
            lib2.setB_name("DBMS");

            StudentOTMUni st1 = new StudentOTMUni();
            st1.setS_name("Andy");

            st1.getBooks_issued().add(lib1);
            st1.getBooks_issued().add(lib2);

            entityManager.persist(st1);

            transaction.commit();
        } finally {
            if (transaction.isActive()) {
                transaction.rollback();
            }
            entityManager.close();
            entityManagerFactory.close();
        }
    }
}
